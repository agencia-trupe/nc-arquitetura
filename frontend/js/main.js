(function($, window, document, undefined) {
    'use strict';

    var App = {

        fetchInstagram: function() {
            var container = $('.instagram-api');

            if(!container.length) return;

            container.hide();
            $.get('fetchInstagram', function(data){
                container.append(data).fadeIn('slow');
            });
        },

        envioContato: function(event) {
            event.preventDefault();

            var form = this;

            $.post('envioContato', {

                assunto  : $('#assunto').val(),
                nome     : $('#nome').val(),
                email    : $('#email').val(),
                mensagem : $('#mensagem').val()

            }, function(resposta){

                if (resposta.status == 'success') form.reset();
                $('.resposta').hide().text(resposta.message).fadeIn('slow');

            }, 'json');
        },

        envioTrabalhe: function(event) {
            event.preventDefault();

            var form = this;

            $.post('envioTrabalhe', {

                categoria : $('#categoria').val(),
                nome      : $('#nome').val(),
                email     : $('#email').val(),
                telefone  : $('#telefone').val(),
                mensagem  : $('#mensagem').val()

            }, function(resposta){

                if (resposta.status == 'success') form.reset();
                $('.resposta').hide().text(resposta.message).fadeIn('slow');

            }, 'json');
        },

        envioOrcamento: function(event) {
            event.preventDefault();

            var form = this;

            $.post('envioOrcamento', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                telefone : $('#telefone').val(),
                area     : $('#area').val(),
                detalhes : $('#detalhes').val()

            }, function(resposta){

                if (resposta.status == 'success') form.reset();
                $('.resposta').hide().text(resposta.message).fadeIn('slow');

            }, 'json');
        },

        init: function() {
            this.fetchInstagram();
            $('#form-contato').submit(this.envioContato);
            $('#form-trabalhe').submit(this.envioTrabalhe);
            $('#form-orcamento').submit(this.envioOrcamento);
        }

    };

    $(document).ready(App.init());

}(jQuery, window, document));
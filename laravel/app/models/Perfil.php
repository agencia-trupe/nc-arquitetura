<?php

class Perfil extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'perfil';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array();

    // array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

}

<?php

class Tools {

    public static function isActive($route, $addClass = false, $class_name = 'active')
    {
        $active = false;

        if (!is_array($route)) $route = array($route);

        foreach ($route as $r) {
            if (Request::is($r . '/*') || Request::is($r)) $active = true;
        }

        if ($active) return ($addClass ? ' class="'.$class_name.'"' : $class_name);
    }

}
<?php

class Instagram {

    private static function get_curl($url) {
        if(function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $output = curl_exec($ch);
            echo curl_error($ch);
            curl_close($ch);
            return $output;
        } else{
            return file_get_contents($url);
        }
    }

    public static function get_images($limit = 5)
    {
        $token   = '1700629245.4c71b4e.962b8ab803c14d64a8dc0e980a9e8a51';
        $user_id = '1575092500';
        $api_url = "https://api.instagram.com/v1/users/".$user_id."/media/recent/?access_token=".$token."&count=".$limit;

        $response = self::get_curl($api_url);
        $json     = json_decode($response);
        $images   = array();

        if (property_exists($json, 'data')) {
            foreach($json->data as $item)
            {
                $thumb = $item->images->low_resolution->url;
                $url   = $item->link;

                $object = new stdClass();

                $object->thumb = $thumb;
                $object->url   = $url;

                $images[] = $object;
            }
            return $images;
        } else {
            return false;
        }
    }

}
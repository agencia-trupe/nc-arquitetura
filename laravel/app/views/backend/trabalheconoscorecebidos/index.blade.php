@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            <small>Trabalhe Conosco /</small> Mensagens Recebidas
        </h2>
    </legend>

    @if (sizeof($trabalheConoscoRecebidos))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Categoria</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Mensagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>

        @foreach ($trabalheConoscoRecebidos as $contato)

            <tr class="tr-row">
                <td>
                    {{ $contato->categoria }}
                </td>
                <td>
                    {{ $contato->nome }}
                </td>
                <td>
                    {{ $contato->email }}
                </td>
                <td>
                    {{ $contato->telefone }}
                </td>
                <td>
                    {{ $contato->mensagem }}
                </td>
                <td class="crud-actions">
                   {{ Form::open(array('route' => array('painel.trabalheConoscoRecebidos.destroy', $contato->id), 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
    {{$trabalheConoscoRecebidos->links()}}
    @else
    <div class="alert alert-warning" role="alert">Nenhuma mensagem recebida.</div>
    @endif


@stop
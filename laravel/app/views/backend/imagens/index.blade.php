@section('content')

    <legend>
        <h2>Imagens</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.imagens.update', $imagens->id), 'files' => true, 'method' => 'put') ) }}
            <div class="pad">

                @if(Session::has('sucesso'))
                    <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
                @endif

                @if($errors->any())
                    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
                @endif

                <div class="form-group">
                    <label for="home" style="display:block">Home <small>(2000x450 px)</small></label>
                    <img src="{{url('assets/img/sistema/'.$imagens->home)}}" alt="" style="width:490px; height auto;">
                    <input type="file" class="form-control" id="home" name="home" style="width: 490px;">
                </div>

                <div class="form-group">
                    <label for="interiores" style="display:block">Arquitetura de Interiores <small>(490x277 px)</small></label>
                    <img src="{{url('assets/img/sistema/'.$imagens->interiores)}}" alt="">
                    <input type="file" class="form-control" id="interiores" name="interiores" style="width: 490px;">
                </div>

                <div class="form-group">
                    <label for="exteriores" style="display:block">Arquitetura de Exteriores <small>(490x277 px)</small></label>
                    <img src="{{url('assets/img/sistema/'.$imagens->exteriores)}}" alt="">
                    <input type="file" class="form-control" id="exteriores" name="exteriores" style="width: 490px;">
                </div>

                <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            </div>
        </form>

@stop
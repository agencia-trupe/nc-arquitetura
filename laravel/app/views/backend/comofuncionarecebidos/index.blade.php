@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            <small>Como Funciona /</small> Orçamentos Recebidos
        </h2>
    </legend>

    @if (sizeof($comoFuncionaRecebidos))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Área quadrada do projeto</th>
                <th>Detalhes</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>

        @foreach ($comoFuncionaRecebidos as $contato)

            <tr class="tr-row">
                <td>
                    {{ $contato->nome }}
                </td>
                <td>
                    {{ $contato->email }}
                </td>
                <td>
                    {{ $contato->telefone }}
                </td>
                <td>
                    {{ $contato->area }}
                </td>
                <td>
                    {{ $contato->detalhes }}
                </td>
                <td class="crud-actions">
                   {{ Form::open(array('route' => array('painel.comoFuncionaRecebidos.destroy', $contato->id), 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
    {{$comoFuncionaRecebidos->links()}}
    @else
    <div class="alert alert-warning" role="alert">Nenhum orçamento recebido.</div>
    @endif


@stop
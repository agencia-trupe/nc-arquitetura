@section('content')

    <legend>
        <h2>Adicionar Usuário</h2>
    </legend>

    <form action="{{URL::route('painel.usuarios.store')}}" method="post">
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputUsuario">Usuário</label>
                <input type="text" class="form-control" id="inputUsuario" name="username"  @if(Session::has('formulario')) value="{{ Session::get('formulario.username') }}" @endif required>
            </div>

            <div class="form-group">
                <label for="inputEmail">E-mail</label>
                <input type="email" class="form-control" id="inputEmail" name="email" @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif>
            </div>

            <div class="form-group">
                <label for="inputSenha">Senha</label>
                <input type="password" class="form-control" id="inputSenha" name="password" required>
            </div>

            <div class="form-group">
                <label for="inputConfSenha">Digite novamente a Senha</label>
                <input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
            </div>

            <button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

            <a href="{{URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop
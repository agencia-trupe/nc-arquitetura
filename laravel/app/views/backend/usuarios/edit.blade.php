@section('content')

    <legend>
        <h2>Editar Usuário</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.usuarios.update', $usuario->id), 'files' => false, 'method' => 'put') ) }}
        <div class="pad">

            @if(Session::has('sucesso'))
               <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputUsuario">Usuário</label>
                <input type="text" class="form-control" id="inputUsuario" name="username" value="{{$usuario->username}}" required>
            </div>

            <div class="form-group">
                <label for="inputEmail">E-mail</label>
                <input type="email" class="form-control" id="inputEmail" name="email" value="{{$usuario->email}}">
            </div>

            <div class="form-group">
                <label for="inputSenha">Senha</label>
                <input type="password" class="form-control" id="inputSenha" name="password" required>
            </div>

            <div class="form-group">
                <label for="inputConfSenha">Digite novamente a Senha</label>
                <input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
            </div>

            <button type="submit" title="Inserir" class="btn btn-success">Alterar</button>

            <a href="{{URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </div>
    </form>

@stop
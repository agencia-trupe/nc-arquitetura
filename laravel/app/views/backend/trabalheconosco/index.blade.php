@section('content')

    <legend>
        <h2>Trabalhe Conosco</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.trabalheConosco.update', $trabalheconosco->id), 'files' => true, 'method' => 'put') ) }}
            <div class="pad">

                @if(Session::has('sucesso'))
                    <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
                @endif

                @if($errors->any())
                    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
                @endif

                <div class="form-group">
                    <label for="inputChamada">Frase de Chamada <small>(opcional)</small></label>
                    <input type="text" class="form-control" id="inputChamada" name="chamada" value="{{$trabalheconosco->chamada}}">
                </div>

                <div class="form-group">
                    <label for="imagem" style="display:block">Imagem <small>(440x270 px)</small></label>
                    <img src="{{url('assets/img/sistema/'.$trabalheconosco->imagem)}}" alt="">
                    <input type="file" class="form-control" id="imagem" name="imagem" style="width: 440px;">
                </div>

                <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            </div>
        </form>

@stop
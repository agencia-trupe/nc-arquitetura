@section('content')

    <legend>
        <h2>Perfil</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.perfil.update', $perfil->id), 'files' => true, 'method' => 'put') ) }}
            <div class="pad">

                @if(Session::has('sucesso'))
                    <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
                @endif

                @if($errors->any())
                    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
                @endif

                <div class="form-group">
                    <label for="texto">Texto</label>
                    <textarea required name="texto" class="form-control ckeditor-noheading" id="texto" rows="6" required>{{$perfil->texto}}</textarea>
                </div>

                <div class="form-group">
                    <label for="destaque">Destaque <small>(box laranja, opcional)</small></label>
                    <textarea required name="destaque" class="form-control ckeditor-noheading" id="destaque" rows="6">{{$perfil->destaque}}</textarea>
                </div>

                <div class="form-group">
                    <label for="imagem" style="display:block">Imagem <small>(475x324 px)</small></label>
                    <img src="{{url('assets/img/sistema/'.$perfil->imagem)}}" alt="">
                    <input type="file" class="form-control" id="imagem" name="imagem" style="width: 475px;">
                </div>

                <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            </div>
        </form>

@stop
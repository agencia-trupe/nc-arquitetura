@section('content')

    <legend>
        <h2>Informações de Contato</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.contato.update', $contato->id), 'files' => true, 'method' => 'put') ) }}
            <div class="pad">

            @if(Session::has('sucesso'))
                <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
            @endif

            @if($errors->any())
                <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
            @endif

            <div class="form-group">
                <label for="inputEmaildeContato">Email de Contato</label>
                <input type="text" class="form-control" id="inputEmaildeContato" name="email" value="{{$contato->email}}" required>
            </div>

            <div class="form-group">
                <label for="inputEmaildeContato">Telefone de Contato <small>(opcional)</small></label>
                <input type="text" class="form-control" id="inputTelefonedeContato" name="telefone" value="{{$contato->telefone}}">
            </div>

            <div class="form-group">
                <label for="inputInstagram">Instagram</label>
                <input type="text" class="form-control" id="inputInstagram" name="instagram" value="{{$contato->instagram}}">
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

        </div>
    </form>

@stop
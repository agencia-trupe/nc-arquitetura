@section('content')

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <legend>
        <h2>
            Contatos Recebidos
        </h2>
    </legend>

    @if (sizeof($contatosRecebidos))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Assunto</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Mensagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>

        @foreach ($contatosRecebidos as $contato)

            <tr class="tr-row">
                <td>
                    {{ $contato->assunto }}
                </td>
                <td>
                    {{ $contato->nome }}
                </td>
                <td>
                    {{ $contato->email }}
                </td>
                <td>
                    {{ $contato->mensagem }}
                </td>
                <td class="crud-actions">
                   {{ Form::open(array('route' => array('painel.contatosRecebidos.destroy', $contato->id), 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
    {{$contatosRecebidos->links()}}
    @else
    <div class="alert alert-warning" role="alert">Nenhum contato recebido.</div>
    @endif


@stop
@section('content')

    <legend>
        <h2>Como Funciona</h2>
    </legend>

    {{ Form::open( array('route' => array('painel.comoFunciona.update', $comofunciona->id), 'files' => false, 'method' => 'put') ) }}
            <div class="pad">

                @if(Session::has('sucesso'))
                    <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
                @endif

                @if($errors->any())
                    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
                @endif

                <div class="form-group">
                    <label for="texto">Descrição</label>
                    <textarea required name="descricao" class="form-control ckeditor-heading" id="descricao" rows="6" required>{{$comofunciona->descricao}}</textarea>
                </div>

                <div class="form-group">
                    <label for="texto">Passo 1</label>
                    <textarea required name="passo1" class="form-control ckeditor-heading" id="passo1" rows="6" required>{{$comofunciona->passo1}}</textarea>
                </div>

                <div class="form-group">
                    <label for="texto">Passo 2</label>
                    <textarea required name="passo2" class="form-control ckeditor-heading" id="passo2" rows="6" required>{{$comofunciona->passo2}}</textarea>
                </div>

                <div class="form-group">
                    <label for="texto">Passo 3</label>
                    <textarea required name="passo3" class="form-control ckeditor-heading" id="passo3" rows="6" required>{{$comofunciona->passo3}}</textarea>
                </div>

                <div class="form-group">
                    <label for="texto">Detalhes</label>
                    <textarea required name="detalhes" class="form-control ckeditor-heading" id="detalhes" rows="6" required>{{$comofunciona->detalhes}}</textarea>
                </div>

                <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            </div>
        </form>

@stop
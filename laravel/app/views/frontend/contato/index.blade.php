@section('content')

    <div class="title">
        <h2 class="contato center">Contato</h2>
    </div>

    <div id="contato" class="center">
        <div class="info">
            <div class="left">
                <p>Aguardamos o seu contato, <br>contrate seu projeto ou tire suas dúvidas</p>
            </div>

            <div class="right">
@if($contato->telefone)
                <p>{{ $contato->telefone }}</p>
@endif
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </div>
        </div>

        <form action="" method="post" id="form-contato">
            <div class="left">
                <input type="text" name="assunto" id="assunto" placeholder="Assunto">
                <input type="text" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
            </div>

            <div class="right">
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar">
                <div class="resposta"></div>
            </div>
        </form>
    </div>

@stop
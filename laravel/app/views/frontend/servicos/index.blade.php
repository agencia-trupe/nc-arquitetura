@section('content')

    <div class="title">
        <h2 class="servicos center">Nossos Serviços</h2>
    </div>

    <div id="servicos" class="center">
        <div class="servicos-wrapper">
            <div class="col interiores">
                <div class="servico">
                    <img src="assets/img/sistema/{{ $imagens->interiores }}" alt="">
                    <h2>Arquitetura de Interiores</h2>
                </div>

                <div class="servico-detalhes">
                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-interiores.png" alt="">
                        <p>Design de Interiores</p>
                    </div>

                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-moveis.png" alt="">
                        <p>Móveis Planejados</p>
                    </div>

                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-reforma.png" alt="">
                        <p>Reforma Interna</p>
                    </div>
                </div>
            </div>

            <div class="col exteriores">
                <div class="servico">
                    <img src="assets/img/sistema/{{ $imagens->exteriores }}" alt="">
                    <h2>Arquitetura de Exteriores</h2>
                </div>

                <div class="servico-detalhes">
                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-fachada.png" alt="">
                        <p>Design de Fachada</p>
                    </div>

                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-externa.png" alt="">
                        <p>Design de Área Externa</p>
                    </div>

                    <div class="detalhe">
                        <img src="assets/img/layout/servicos-paisagismo.png" alt="">
                        <p>Paisagismo</p>
                    </div>
                </div>
            </div>

            <h3>O que será entregue</h3>
            <div class="entregue">
                <div class="entregue-item">
                    <img src="assets/img/layout/servicos-2d.png" alt="">
                    <p>
                        Imagem 2D
                        <span>(Desenho Técnico)</span>
                    </p>
                </div>

                <div class="entregue-mais"></div>

                <div class="entregue-item">
                    <img src="assets/img/layout/servicos-3d.png" alt="">
                    <p>
                        Imagens 3D
                        <span>(Maquete eletrônica)</span>
                    </p>
                </div>

                <div class="entregue-mais"></div>

                <div class="entregue-item">
                    <img src="assets/img/layout/servicos-especificacoes.png" alt="">
                    <p>Especificações</p>
                </div>
            </div>

            <div class="entenda">
                <a href="como-funciona">Clique aqui e entenda como funciona!</a>
            </div>
        </div>
    </div>

@stop
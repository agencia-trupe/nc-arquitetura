@section('content')

    <div class="banner" style="background-image: url('assets/img/sistema/{{ $imagens->home }}')"></div>

    <div id="home" class="center">
        <div class="servicos-wrapper">
            <div class="servico interiores">
                <img src="assets/img/sistema/{{ $imagens->interiores }}" alt="">
                <h2>Arquitetura de Interiores</h2>
            </div>

            <div class="servico exteriores">
                <img src="assets/img/sistema/{{ $imagens->exteriores }}" alt="">
                <h2>Arquitetura de Exteriores</h2>
            </div>

            <div class="botao-wrapper">
                <a href="nossos-servicos" class="conheca">Conheça nossos serviços</a>
            </div>
        </div>

        <div class="instagram-api">
            <a href="http://instagram.com/nc_arquitetura" class="userlink" target="_blank">@nc_arquitetura</a>
        </div>
    </div>

@stop
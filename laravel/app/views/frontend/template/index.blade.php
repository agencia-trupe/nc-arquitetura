<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Design">
    <meta name="copyright" content="2015 Trupe Design">

    <meta name="description" content="Escritório virtual de arquitetura, com foco em projetos de arquitetura personalizados, paisagismo e interiores, orçamentos online.">
    <meta name="keywords" content="projetos arquitetura, arquitetos, arquitetura, paisagismo, interiores, projetos personalizados de arquitetura, escritorio arquitetura, arquiteta, arquitetura de interiores, arquitetura de exteriores, design arquitetura, reforma, paisagismo, orçamento arquitetura">
    <meta property="og:title" content="NC Arquitetura">
    <meta property="og:description" content="Escritório virtual de arquitetura, com foco em projetos de arquitetura personalizados, paisagismo e interiores, orçamentos online.">
    <meta property="og:site_name" content="NC Arquitetura">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ url('assets/img/layout/logo.png') }}">

    <title>NC Arquitetura</title>

    <link rel="stylesheet" href="{{ url('assets/css/main.css') }}">
</head>
<body>
    <header>
        <div class="center">
            <h1><a href="/">NC Arquitetura</a></h1>

            <nav>
                <a href="/"{{ Tools::isActive(['home', '/'], true) }}>Home</a>
                <a href="perfil"{{ Tools::isActive('perfil', true) }}>Perfil</a>
                <a href="nossos-servicos"{{ Tools::isActive('nossos-servicos', true) }}>Nossos Serviços</a>
                <a href="como-funciona"{{ Tools::isActive('como-funciona', true) }}>Como Funciona</a>
                <a href="trabalhe-conosco"{{ Tools::isActive('trabalhe-conosco', true) }}>Trabalhe Conosco</a>
                <a href="contato"{{ Tools::isActive('contato', true) }}>Contato</a>
@if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="instagram">Instagram</a>
@endif
            </nav>
        </div>
    </header>
@yield('content')
    <footer>
        <div class="center">
            <nav>
                <a href="/">Home</a>
                <a href="perfil">Perfil</a>
                <a href="nossos-servicos">Nossos Serviços</a>
                <a href="como-funciona">Como Funciona</a>
                <a href="trabalhe-conosco">Trabalhe Conosco</a>
                <a href="contato">Contato</a>
            </nav>

            <div class="info">
@if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="instagram">Instagram</a>
@endif
@if($contato->telefone)
                <p>{{ $contato->telefone }}</p>
@endif
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                <a href="http://trupe.net" target="_blank" class="trupe">Trupe</a>
            </div>

            <div class="copyright">
                <img src="{{ url('assets/img/layout/logo-footer.png') }}" alt="">
                <p>©2015 NC Arquitetura - Todos os direitos reservados.</p>
            </div>
        </div>
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-2.1.3.min.js"><\/script>')</script>
    <script src="{{ url('assets/js/main.js') }}"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-60191687-1', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>
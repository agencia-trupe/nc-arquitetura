@section('content')

    <div class="title">
        <h2 class="comofunciona center">Como Funciona</h2>
    </div>

    <div id="comofunciona">
        <div class="center">
            <div class="descricao">
                {{ $comofunciona->descricao }}
            </div>

            <div class="passo-a-passo">
                <div class="passo1">
                    <img src="assets/img/layout/comofunciona-passo1.png" alt="">
                    {{ $comofunciona->passo1 }}
                </div>

                <div class="passo2">
                    <img src="assets/img/layout/comofunciona-passo2.png" alt="">
                    {{ $comofunciona->passo2 }}
                </div>

                <div class="passo3">
                    <img src="assets/img/layout/comofunciona-passo3.png" alt="">
                    {{ $comofunciona->passo3 }}
                </div>
            </div>
        </div>

        <div class="divisoria"></div>

        <div class="center">
            <div class="detalhes">
                {{ $comofunciona->detalhes }}
            </div>

            <form action="" method="post" id="form-orcamento">
                <h3>Solicite um Orçamento</h3>
                <input type="text" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="Telefone">
                <input type="text" name="area" id="area" placeholder="Área quadrada do projeto">
                <textarea name="detalhes" id="detalhes" placeholder="Detalhes" required></textarea>
                <input type="submit" value="Enviar">
                <div class="resposta"></div>
            </form>
        </div>
    </div>

@stop
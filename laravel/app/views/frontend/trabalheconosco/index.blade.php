@section('content')

    <div class="title">
        <h2 class="trabalhe center">Trabalhe Conosco</h2>
    </div>

    <div id="trabalhe" class="center">
        <div class="chamada">
@if($trabalheconosco->chamada)
            <p>{{ $trabalheconosco->chamada }}</p>
@endif
            <img src="assets/img/sistema/{{ $trabalheconosco->imagem }}" alt="">
        </div>

        <form action="" method="post" id="form-trabalhe">
            <select name="categoria" id="categoria" required>
                <option value="Lojas e fornecedores">Lojas e fornecedores</option>
                <option value="Arquitetos">Arquitetos</option>
                <option value="Prestadores de serviço">Prestadores de serviço</option>
            </select>
            <input type="text" name="nome" id="nome" placeholder="Nome / Responsável" required>
            <input type="email" name="email" id="email" placeholder="E-mail" required>
            <input type="text" name="telefone" id="telefone" placeholder="Telefone">
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
            <input type="submit" value="Enviar">
            <div class="resposta"></div>
        </form>
    </div>

@stop
@section('content')

    <div class="title">
        <h2 class="perfil center">Perfil</h2>
    </div>

    <div id="perfil" class="center">
        <img src="assets/img/sistema/{{ $perfil->imagem }}" alt="">

        <div class="texto">
            {{ $perfil->texto }}
        </div>

@if($perfil->destaque)
        <div class="destaque">
            {{ $perfil->destaque }}
        </div>
@endif
    </div>

@stop
<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] NC Arquitetura</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Área quadrada do projeto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $area }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Detalhes:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $detalhes }}</span>
</body>
</html>

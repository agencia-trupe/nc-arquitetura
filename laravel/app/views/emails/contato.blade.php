<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] NC Arquitetura</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Assunto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $assunto }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#f9a52b;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>

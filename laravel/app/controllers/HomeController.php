<?php

use \Imagens;

class HomeController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{
        $imagens = Imagens::first();
        $this->layout->content = View::make('frontend.home.index')
            ->with(compact('imagens'));
	}

}

<?php

use \ComoFunciona, \Contato;

class ComoFuncionaController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{
        $comofunciona = ComoFunciona::first();
        $this->layout->content = View::make('frontend.comofunciona.index')
            ->with(compact('comofunciona'));
	}

    public function envio()
    {
        $contato = Contato::first();

        $nome = Input::get('nome');
        $email = Input::get('email');
        $telefone = Input::get('telefone');
        $area = Input::get('area');
        $detalhes = Input::get('detalhes');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'detalhes' => $detalhes
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'detalhes' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Preencha corretamente todos os campos!'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'nome' => $nome,
                'email' => $email,
                'telefone' => $telefone,
                'area' => $area,
                'detalhes' => $detalhes
            );

            Mail::send('emails.orcamento', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, 'NC Arquitetura')
                        ->cc('naiannafc@hotmail.com')
                        ->subject('[ORÇAMENTO] NC Arquitetura')
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ComoFuncionaRecebido;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->area = $area;
        $object->detalhes = $detalhes;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        );
        return Response::json($response);
    }

}

<?php

use \Contato;

class ContatoController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{
        $contato = Contato::first();
        $this->layout->content = View::make('frontend.contato.index')
            ->with(compact('contato'));
	}

    public function envio()
    {
        $contato = Contato::first();

        $assunto = Input::get('assunto');
        $nome = Input::get('nome');
        $email = Input::get('email');
        $mensagem = Input::get('mensagem');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'mensagem' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Preencha corretamente todos os campos!'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'assunto' => $assunto,
                'nome' => $nome,
                'email' => $email,
                'mensagem' => $mensagem
            );

            Mail::send('emails.contato', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, 'NC Arquitetura')
                        ->cc('naiannafc@hotmail.com')
                        ->subject('[CONTATO] NC Arquitetura')
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ContatoRecebido;
        $object->assunto = $assunto;
        $object->nome = $nome;
        $object->email = $email;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        );
        return Response::json($response);
    }

}

<?php

use \Perfil;

class PerfilController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{
        $perfil = Perfil::first();
        $this->layout->content = View::make('frontend.perfil.index')
            ->with(compact('perfil'));
	}

}

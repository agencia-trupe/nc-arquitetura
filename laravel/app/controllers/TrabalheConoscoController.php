<?php

use \TrabalheConosco, \Contato;

class TrabalheConoscoController extends BaseController {

	protected $layout = 'frontend.template.index';

	public function index()
	{
        $trabalheconosco = TrabalheConosco::first();
        $this->layout->content = View::make('frontend.trabalheconosco.index')
            ->with(compact('trabalheconosco'));
	}

    public function envio()
    {
        $contato = Contato::first();

        $categoria = Input::get('categoria');
        $nome = Input::get('nome');
        $email = Input::get('email');
        $telefone = Input::get('telefone');
        $mensagem = Input::get('mensagem');

        $validation = Validator::make(
            array(
                'categoria' => $categoria,
                'nome'      => $nome,
                'email'     => $email,
                'mensagem'  => $mensagem
            ),
            array(
                'categoria' => 'required',
                'nome'      => 'required',
                'email'     => 'required|email',
                'mensagem'  => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Preencha corretamente todos os campos!'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'categoria' => $categoria,
                'nome' => $nome,
                'email' => $email,
                'telefone' => $telefone,
                'mensagem' => $mensagem
            );

            Mail::send('emails.trabalhe', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, 'NC Arquitetura')
                        ->cc('naiannafc@hotmail.com')
                        ->subject('[TRABALHE CONOSCO] NC Arquitetura')
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new TrabalheConoscoRecebido;
        $object->categoria = $categoria;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        );
        return Response::json($response);
    }

}

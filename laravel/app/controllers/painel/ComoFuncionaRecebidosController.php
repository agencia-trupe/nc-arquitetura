<?php

namespace Painel;

use \ComoFuncionaRecebido, \View, \Input, \Session, \Redirect;

class ComoFuncionaRecebidosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.comofuncionarecebidos.index')
            ->with('comoFuncionaRecebidos', ComoFuncionaRecebido::orderBy('id', 'desc')->paginate(10));
    }

    public function store() {}
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $object = ComoFuncionaRecebido::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Orçamento removido com sucesso.');

        return Redirect::route('painel.comoFuncionaRecebidos.index');
    }

}
<?php

namespace Painel;

use \TrabalheConosco, \View, \Input, \Session, \Redirect, \Image, \Validator;

class TrabalheConoscoController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.trabalheconosco.index')
            ->with('trabalheconosco', TrabalheConosco::first());
    }

    public function update($id)
    {
        $object = TrabalheConosco::find($id);

        $object->chamada = Input::get('chamada');

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem');

            $rules = array('imagem' => 'image');
            $validation = Validator::make(array('imagem' => $imagem), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/sistema/'.$name;

                $imgobj = Image::make(Input::file('imagem')->getRealPath());
                $imgobj->fit(440, 270, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Página alterada com sucesso.');
            return Redirect::route('painel.trabalheConosco.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar página!'));

        }
    }

    public function destroy() {}

}
<?php

namespace Painel;

use \Contato, \View, \Input, \Session, \Redirect;

class ContatoController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.contato.index')
            ->with('contato', Contato::first());
    }

    public function create() {}
    public function store() {}
    public function show() {}
    public function edit() {}

    public function update($id)
    {
        $object = Contato::find($id);

        $object->email = Input::get('email');
        $object->telefone = Input::get('telefone');
        $object->instagram = Input::get('instagram');

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Informações de Contato alteradas com sucesso.');
            return Redirect::route('painel.contato.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao criar Informações de Contato!'));

        }
    }

    public function destroy($id) {}

}
<?php

namespace Painel;

use \ContatoRecebido, \View, \Input, \Session, \Redirect;

class ContatosRecebidosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.contatosrecebidos.index')
            ->with('contatosRecebidos', ContatoRecebido::orderBy('id', 'desc')->paginate(10));
    }

    public function store() {}
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $object = ContatoRecebido::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Contato removido com sucesso.');

        return Redirect::route('painel.contatosRecebidos.index');
    }

}
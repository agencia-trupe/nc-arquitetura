<?php

namespace Painel;

use \Imagens, \View, \Input, \Session, \Redirect, \Image, \Validator;

class ImagensController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.imagens.index')
            ->with('imagens', Imagens::first());
    }

    public function update($id)
    {
        $object = Imagens::find($id);

        if (Input::hasFile('home')) {

            $home = Input::file('home');

            $rules = array('home' => 'image');
            $validation = Validator::make(array('home' => $home), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$home->getClientOriginalName();
                $path = 'assets/img/sistema/'.$name;

                $imgobj = Image::make(Input::file('home')->getRealPath());
                $imgobj->fit(2000, 450, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->home = $name;
            }

        }

        if (Input::hasFile('interiores')) {

            $interiores = Input::file('interiores');

            $rules = array('interiores' => 'image');
            $validation = Validator::make(array('interiores' => $interiores), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$interiores->getClientOriginalName();
                $path = 'assets/img/sistema/'.$name;

                $imgobj = Image::make(Input::file('interiores')->getRealPath());
                $imgobj->fit(490, 277, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->interiores = $name;
            }

        }

        if (Input::hasFile('exteriores')) {

            $exteriores = Input::file('exteriores');

            $rules = array('exteriores' => 'image');
            $validation = Validator::make(array('exteriores' => $exteriores), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$exteriores->getClientOriginalName();
                $path = 'assets/img/sistema/'.$name;

                $imgobj = Image::make(Input::file('exteriores')->getRealPath());
                $imgobj->fit(490, 277, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->exteriores = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Imagens alteradas com sucesso.');
            return Redirect::route('painel.imagens.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Imagens!'));

        }
    }

    public function destroy() {}

}
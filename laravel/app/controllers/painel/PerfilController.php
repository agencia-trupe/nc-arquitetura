<?php

namespace Painel;

use \Perfil, \View, \Input, \Session, \Redirect, \Image, \Validator;

class PerfilController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.perfil.index')
            ->with('perfil', Perfil::first());
    }

    public function update($id)
    {
        $object = Perfil::find($id);

        $object->texto = Input::get('texto');
        $object->destaque = Input::get('destaque');

        if (Input::hasFile('imagem')) {

            $imagem = Input::file('imagem');

            $rules = array('imagem' => 'image');
            $validation = Validator::make(array('imagem' => $imagem), $rules);

            if ($validation->fails()) {
                return Redirect::back()->withErrors(array('Erro: O arquivo deve ser uma imagem!'));
            } else {
                $name = date('YmdHis').$imagem->getClientOriginalName();
                $path = 'assets/img/sistema/'.$name;

                $imgobj = Image::make(Input::file('imagem')->getRealPath());
                $imgobj->fit(475, 324, function ($constraint) {
                    $constraint->upsize();
                })->save($path, 100);

                $object->imagem = $name;
            }

        }

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Perfil alterado com sucesso.');
            return Redirect::route('painel.perfil.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Perfil!'));

        }
    }

    public function destroy() {}

}
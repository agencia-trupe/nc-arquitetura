<?php

namespace Painel;

use \ComoFunciona, \View, \Input, \Session, \Redirect, \Image, \Validator;

class ComoFuncionaController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.comofunciona.index')
            ->with('comofunciona', ComoFunciona::first());
    }

    public function update($id)
    {
        $object = ComoFunciona::find($id);

        $object->descricao = Input::get('descricao');
        $object->passo1 = Input::get('passo1');
        $object->passo2 = Input::get('passo2');
        $object->passo3 = Input::get('passo3');
        $object->detalhes = Input::get('detalhes');

        try {

            $object->save();
            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Página alterada com sucesso.');
            return Redirect::route('painel.comoFunciona.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(array('Erro ao alterar Página!'));

        }
    }

    public function destroy() {}

}
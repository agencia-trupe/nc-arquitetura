<?php

namespace Painel;

use \TrabalheConoscoRecebido, \View, \Input, \Session, \Redirect;

class TrabalheConoscoRecebidosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.trabalheconoscorecebidos.index')
            ->with('trabalheConoscoRecebidos', TrabalheConoscoRecebido::orderBy('id', 'desc')->paginate(10));
    }

    public function store() {}
    public function show() {}
    public function edit($id) {}
    public function update($id) {}

    public function destroy($id) {
        $object = TrabalheConoscoRecebido::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Mensagem removida com sucesso.');

        return Redirect::route('painel.trabalheConoscoRecebidos.index');
    }

}
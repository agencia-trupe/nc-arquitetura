<?php

namespace Painel;

use \User, \View, \Input, \Session, \Redirect, \Hash;

class UsuariosController extends BaseAdminController {

    protected $layout = 'backend.template.index';

    public function index()
    {
        $this->layout->content = View::make('backend.usuarios.index')
            ->with('usuarios', User::all());
    }

    public function create()
    {
        $this->layout->content = View::make('backend.usuarios.form');
    }

    public function store()
    {
        $object = new User;

        $object->email = Input::get('email');
        $object->username = Input::get('username');
        $object->password = Hash::make(Input::get('password'));

        if (!Input::get('username') || !Input::get('password') || (Input::get('password') != Input::get('password_confirm'))){
            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
        } else {
            try {

                $object->save();
                Session::flash('sucesso', true);
                Session::flash('mensagem', 'Usuário criado com sucesso.');
                return Redirect::route('painel.usuarios.index');

            } catch (\Exception $e) {

                Session::flash('formulario', Input::all());
                return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));

            }
        }
    }

    public function show($id) {}

    public function edit($id)
    {
        $this->layout->content = View::make('backend.usuarios.edit')
            ->with('usuario', User::find($id));
    }

    public function update($id)
    {
        $object = User::find($id);

        $object->email = Input::get('email');
        $object->username = Input::get('username');

        if (Input::has('password')) {
            if (Input::get('password') == Input::get('password_confirm')) {
                $object->password = Hash::make(Input::get('password'));
            } else {
                Session::flash('formulario', Input::all());
                return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
            }
        }

        if (!Input::get('username')) {
            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
        } else {
            try {

                $object->save();
                Session::flash('sucesso', true);
                Session::flash('mensagem', 'Usuário alterado com sucesso.');
                return Redirect::route('painel.usuarios.index');

            } catch (\Exception $e) {

                Session::flash('formulario', Input::all());
                return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));

            }
        }
    }

    public function destroy($id) {
        $object = User::find($id);
        $object->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Usuário removido com sucesso.');

        return Redirect::route('painel.usuarios.index');
    }

}
<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'email' => 'contato@ncarquitetura.com.br',
                'telefone' => '(69) 9908.6066',
                'instagram' => 'http://instagram.com/nc_arquitetura'
            )
        );

        DB::table('contato')->insert($data);
    }

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComoFuncionaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('como_funciona', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('descricao');
			$table->text('passo1');
			$table->text('passo2');
			$table->text('passo3');
			$table->text('detalhes');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('como_funciona');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabalheConoscoRecebidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trabalhe_conosco_recebidos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('categoria');
			$table->string('nome');
			$table->string('email');
			$table->string('telefone');
			$table->text('mensagem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trabalhe_conosco_recebidos');
	}

}

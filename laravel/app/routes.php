<?php

// Front

Route::get('/', array(
    'as'   => 'home',
    'uses' => 'HomeController@index'
));

Route::get('home', array(
    'as'   => 'home',
    'uses' => 'HomeController@index'
));

Route::get('fetchInstagram', function(){
    if (!Request::ajax()) return;

    $instagram_imagens = Instagram::get_images();
    return View::make('frontend.home.instagram', compact('instagram_imagens'));
});

Route::get('perfil', array(
    'as'   => 'perfil',
    'uses' => 'PerfilController@index'
));

Route::get('nossos-servicos', array(
    'as'   => 'nossos-servicos',
    'uses' => 'ServicosController@index'
));

Route::get('como-funciona', array(
    'as'   => 'como-funciona',
    'uses' => 'ComoFuncionaController@index'
));

Route::post('envioOrcamento', array(
    'as'   => 'como-funciona.envio',
    'uses' => 'ComoFuncionaController@envio'
));

Route::get('trabalhe-conosco', array(
    'as'   => 'trabalhe-conosco',
    'uses' => 'TrabalheConoscoController@index'
));

Route::post('envioTrabalhe', array(
    'as'   => 'trabalhe-conosco.envio',
    'uses' => 'TrabalheConoscoController@envio'
));

Route::get('contato', array(
    'as'   => 'contato',
    'uses' => 'ContatoController@index'
));

Route::post('envioContato', array(
    'as'   => 'contato.envio',
    'uses' => 'ContatoController@envio'
));


// Painel

Route::get('painel', array(
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
));

Route::get('painel/login', array(
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
));

Route::post('painel/login',  array('as' => 'painel.auth', function() {
    $authvars = array(
        'username' => Input::get('username'),
        'password' => Input::get('password')
    );

    if (Auth::attempt($authvars)) return Redirect::to('painel');

    Session::flash('login_errors', true);
    return Redirect::to('painel/login');
}));

Route::get('painel/logout', array('as' => 'painel.off', function() {
    Auth::logout();
    return Redirect::to('painel');
}));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function() {
    Route::resource('imagens', 'Painel\ImagensController');
    Route::resource('perfil', 'Painel\PerfilController');
    Route::resource('comoFunciona', 'Painel\ComoFuncionaController');
    Route::resource('comoFuncionaRecebidos', 'Painel\ComoFuncionaRecebidosController');
    Route::resource('trabalheConosco', 'Painel\TrabalheConoscoController');
    Route::resource('trabalheConoscoRecebidos', 'Painel\TrabalheConoscoRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');
    Route::resource('contatosRecebidos', 'Painel\ContatosRecebidosController');
    Route::resource('usuarios', 'Painel\UsuariosController');
});
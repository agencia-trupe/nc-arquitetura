$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	var result = confirm("Deseja Excluir o Registro?");
      	if(result) form.submit();
  	});

    if ($('.ckeditor-noheading').length) {
        $('.ckeditor-noheading').each(function (i, obj) {
            CKEDITOR.replace(obj.id, {
                toolbar: [['Bold', 'Italic']]
            });
        })
    }

    if ($('.ckeditor-heading').length) {
        $('.ckeditor-heading').each(function (i, obj) {
            CKEDITOR.replace(obj.id, {
                format_tags : 'p;h3',
                toolbar: [['Bold', 'Italic'], ['Format']]
            });
        })
    }

});